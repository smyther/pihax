'use strict';

describe('Controller: KeyCtrl', function () {

  // load the controller's module
  beforeEach(module('haxApp'));

  var KeyCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    KeyCtrl = $controller('KeyCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(KeyCtrl.awesomeThings.length).toBe(3);
  });
});
