'use strict';

describe('Service: DataFactory', function () {

  // load the service's module
  beforeEach(module('haxApp'));

  // instantiate service
  var DataFactory;
  beforeEach(inject(function (_DataFactory_) {
    DataFactory = _DataFactory_;
  }));

  it('should do something', function () {
    expect(!!DataFactory).toBe(true);
  });

});
