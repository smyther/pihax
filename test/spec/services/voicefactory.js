'use strict';

describe('Service: VoiceFactory', function () {

  // load the service's module
  beforeEach(module('haxApp'));

  // instantiate service
  var VoiceFactory;
  beforeEach(inject(function (_VoiceFactory_) {
    VoiceFactory = _VoiceFactory_;
  }));

  it('should do something', function () {
    expect(!!VoiceFactory).toBe(true);
  });

});
