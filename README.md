# hax

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.12.1.

## Setup

Run `npm install` then `bower install`. I've /probably/ got the deps right...

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
