'use strict';

/**
 * @ngdoc function
 * @name haxApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the haxApp
 */
angular.module('haxApp')
  .controller('UserCtrl', function ($scope, DataFactory) {
        $scope.users = DataFactory.users;
  });
