'use strict';

/**
 * @ngdoc function
 * @name haxApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the haxApp
 */
angular.module('haxApp')
  .controller('LoginCtrl', function ($scope, $routeParams, DataFactory, VoiceFactory, SpotifyFactory  ) {

        angular.forEach(DataFactory, function(user){

          if (user.cardId){
            if (user.cardId === $routeParams.id){

              $scope.user = user;

              if (user.signedIn){
                user.signedIn = false;
                user.signOut = new Date().toString();
                VoiceFactory.say('Bye ' + user.displayName + ', have a nice evening!');
              } else {
                user.signedIn = true;
                user.signIn = new Date().toString();
                VoiceFactory.say('Hello ' + user.displayName + ', how are you today?');
                SpotifyFactory.getTrackFromURI(user.song.uri, 0);
              }





              DataFactory.$save(user);

              //$location.path('/');



            }
          }
        });

  });
