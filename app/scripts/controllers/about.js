'use strict';

/**
 * @ngdoc function
 * @name haxApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the haxApp
 */
angular.module('haxApp')
    .controller('AboutCtrl', function () {
        this.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];
    });
