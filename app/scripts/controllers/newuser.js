'use strict';

/**
 * @ngdoc function
 * @name haxApp.controller:NewuserCtrl
 * @description
 * # NewuserCtrl
 * Controller of the haxApp
 */
angular.module('haxApp')
    .controller('NewUserCtrl', function ($scope, DataFactory, VoiceFactory, $location, SpotifyFactory) {

        $scope.buttonText = 'Record';

        $scope.recognisedName = false;

        VoiceFactory.recognition.onresult = function (result) {
            var name = result.results[0][0].transcript;
            $scope.recognisedName = name;
            $scope.$apply();
        };

        $scope.startRecognition = function () {
            $scope.buttonText = 'Recording..';
            VoiceFactory.recognition.start();
        };

        $scope.endRecognition = function () {
            $scope.buttonText = 'Record';
            VoiceFactory.recognition.stop();
        };

        $scope.save = function () {
            DataFactory.users.$add({
                transcribedName: false,
                displayName: false,
                email: false,
                fullname: $scope.fullname,
                signIn: 'Never',
                signOut: 'Never',
                signedIn: false,
                song: false,
                birthday: false,
                imageUrl: 'https://openclipart.org/image/2400px/svg_to_png/177482/ProfilePlaceholderSuit.png'
            }).then(function (result, xyz) {

                //console.log();
                //VoiceFactory.say('Thanks for signing up, ' + $scope.name);
                $location.path('/user/' + result.path.o[1]);

            });
        };


    });
