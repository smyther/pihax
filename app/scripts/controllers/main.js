'use strict';

/**
 * @ngdoc function
 * @name haxApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the haxApp
 */
angular.module('haxApp')
    .controller('MainCtrl', function ($scope, DataFactory, $filter, SpotifyFactory) {


//        SpotifyFactory.setup();

        $scope.c = 0;

        $scope.data = DataFactory;

        $scope.data.users.$watch(function(){

            angular.forEach($scope.data.users, function(person){

                $scope.c++;
                if (person.signIn !== 'Never'){
                    person.signInText = $filter('date')( new Date(person.signIn), 'EEEE @ hh:mma');
                } else {
                    person.signInText = 'Never';
                }

                if (person.signOut !== 'Never'){
                    person.signOutText = $filter('date')( new Date(person.signOut), 'EEEE @ hh:mma');
                } else {
                    person.signOutText = 'Never';
                }
            });
        });


        $scope.data.guests.$watch(function(){

            angular.forEach($scope.data.guests, function(person){

                $scope.c++;
                if (person.signIn !== 'Never'){
                    person.signInText = $filter('date')( new Date(person.signIn), 'EEEE @ hh:mma');
                } else {
                    person.signInText = 'Never';
                }

                if (person.signOut !== 'Never'){
                    person.signOutText = $filter('date')( new Date(person.signOut), 'EEEE @ hh:mma');
                } else {
                    person.signOutText = 'Never';
                }
            });
        });

    });
