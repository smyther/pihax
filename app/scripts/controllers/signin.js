'use strict';

/**
 * @ngdoc function
 * @name haxApp.controller:SigninCtrl
 * @description
 * # SigninCtrl
 * Controller of the haxApp
 */
angular.module('haxApp')
    .controller('SignInCtrl', function ($scope, VoiceFactory, DataFactory, $location) {

        $scope.recognition = VoiceFactory.recognition;

        $scope.start = function () {
            $scope.recognition.start();
        };

        $scope.stop = function () {
            $scope.recognition.stop();
        };

        $scope.recognition.onresult = function (result) {

            $scope.transcribedName = result.results[0][0].transcript;

            angular.forEach(DataFactory, function (data, iterator) {

                $scope.found = false;

                if (data.transcribedName === $scope.transcribedName && $scope.found === false) {

                    $scope.person = data;
                    $scope.found = true;

                    console.log($scope.person, data);

                    if (data.signedIn){
                        data.signedIn = false;
                        data.signOut = new Date().toString();
                        VoiceFactory.say('Bye ' + $scope.person.displayName + ', have a nice evening!');
                    } else {
                        data.signedIn = true;
                        data.signIn = new Date().toString();
                        VoiceFactory.say('Hello ' + $scope.person.displayName + ', how are you today?');
                    }

                    $scope.$apply();

                    DataFactory.$save(data);

                    $location.path('/');

                }

                if (iterator === DataFactory.length - 1 && !$scope.found){
                    $scope.error = true;
                }
            });

            $scope.$apply();

        };


    });
