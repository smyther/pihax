'use strict';

/**
 * @ngdoc function
 * @name haxApp.controller:KeyCtrl
 * @description
 * # KeyCtrl
 * Controller of the haxApp
 */
angular.module('haxApp')
  .controller('KeyCtrl', function ($scope, $location, $window, $timeout, SpotifyFactory) {

      var buffer = '';

      $scope.url = '';


       $scope.changePage = function(){
         $location.path($scope.url);

         $timeout(function(){
           $location.path('/');
         }, 3000);
       };

       $window.addEventListener('keyup', function(e){

         if (e.keyCode !== 13){
           buffer += String.fromCharCode(e.keyCode);
         } else {

           var string = buffer;

           buffer = '';

           $scope.url = '/login/' + string;

           $scope.changePage();
           $scope.$apply();
         }

       });

  });
