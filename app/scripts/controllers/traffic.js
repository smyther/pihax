'use strict';

/**
 * @ngdoc function
 * @name haxApp.controller:TrafficCtrl
 * @description
 * # TrafficCtrl
 * Controller of the haxApp
 */
angular.module('haxApp')
    .controller('TrafficCtrl', function ($scope, $rootScope, $timeout, $http) {


        $scope.doMap = function () {

            $scope.markers = [];
            $scope.geocoder = new google.maps.Geocoder();
            $scope.autocomplete = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));
            $scope.directions = new google.maps.DirectionsService();

            $scope.office = new google.maps.LatLng(52.446918, -1.723139);

            $scope.map = new google.maps.Map(document.getElementById('map'), {
                center: $scope.office,
                zoom: 12
            });

            $scope.autocomplete.bindTo('bounds', $scope.map);


            $scope.markers.push(new google.maps.Marker({
                map: $scope.map,
                position: $scope.office
            }));

            $scope.map.setCenter($scope.office);

        };

        if (!window.initMap) {
            window.initMap = function () {
                $rootScope.mapLibLoaded = true;
                $scope.doMap();
            };
        }


        if (!$scope.mapLibLoaded) {
            $.getScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyD0h8BNv59fJGrmnOAT5sMmGXyzycXB1SM&callback=initMap&libraries=places,geometry');
        } else {
            $scope.doMap();
        }


        $scope.getLocation = function () {

            $timeout(function () {

                $scope.place = $scope.autocomplete.getPlace();

                $scope.home = $scope.place.geometry.location;

                $scope.markers.push(new google.maps.Marker({
                    map: $scope.map,
                    position: $scope.home
                }));


                var bounds = new google.maps.LatLngBounds();


                bounds.extend($scope.office);
                bounds.extend($scope.home);

                $scope.map.fitBounds(bounds);

                var thing;

                if ($scope.driving === 1) {
                    thing = 'TRANSIT';
                } else {
                    thing = 'DRIVING';
                }

                //var d = new Date(Date.now() + 42000000);

                var request = {
                    origin: $scope.office,
                    destination: $scope.home,
                    travelMode: google.maps.TravelMode[thing],
                    provideRouteAlternatives: true,
                    unitSystem: google.maps.UnitSystem.IMPERIAL,
                    drivingOptions: {
                        //departureTime: new Date(),
                        departureTime: new Date(),
                        trafficModel: google.maps.TrafficModel.BEST_GUESS
                    }//,
                    //transitOptions: {
                    //    modes: [
                    //        google.maps.TransitMode.BUS,
                    //        google.maps.TransitMode.RAIL,
                    //        google.maps.TransitMode.SUBWAY,
                    //        google.maps.TransitMode.TRAIN,
                    //        google.maps.TransitMode.TRAM
                    //    ]
                    //}
                };

                $scope.directions.route(request, function (result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {


                        console.log(result);
                        console.log(result.request.destination.toString());
                        console.log(result.request.origin.toString());
                        $scope.directionResults = result;
                        $scope.$apply();

                        $http.jsonp('https://dev.virtualearth.net/REST/v1/Routes?maxSolns=3&wayPoint.1=' + result.request.origin.toString().replace(')', '').replace('(', '') + '&wayPoint.2=' + result.request.destination.toString().replace(')', '').replace('(', '') + '&key=Al0B2E3X-ICeIjzjjCXAdJMgJy9_Tmr6U87IxDrNrOKk1EEQy9gSqGse1R7JQSZ7&jsonp=JSON_CALLBACK').success(function (a) {
                            console.log(a);

                            $scope.bing = a;

                            angular.forEach(a.resourceSets, function (bam) {

                                angular.forEach(bam.resources[0].routeLegs, function (e) {

                                    angular.forEach(e.itineraryItems, function (i) {
                                        if (i.warnings) {
                                            console.log(i.warnings);

                                            angular.forEach(i.warnings, function (warning) {

                                                var rad = new google.maps.MVCArray();
                                                var p = new google.maps.Polyline({
                                                    map: $scope.map,
                                                    path: rad
                                                });

                                                console.log(warning.severity);
                                                new google.maps.Marker({
                                                    map: $scope.map,
                                                    position: new google.maps.LatLng(warning.origin.split(',')[0], warning.origin.split(',')[1])
                                                });
                                                new google.maps.Marker({
                                                    map: $scope.map,
                                                    position: new google.maps.LatLng(warning.to.split(',')[0], warning.to.split(',')[1])
                                                });
                                            });
                                        }
                                    });
                                });

                            });
                        });


                    }
                });


            }, 200);
        };


    });
