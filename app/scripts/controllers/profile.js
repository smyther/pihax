'use strict';

/**
 * @ngdoc function
 * @name haxApp.controller:NewuserCtrl
 * @description
 * # NewuserCtrl
 * Controller of the haxApp
 */
angular.module('haxApp')
    .controller('ProfileCtrl', function ($scope, $http, $routeParams, $route, DataFactory, $location, VoiceFactory) {


  

        $scope.newUser = false;

        if ($route.current.$$route.newUser){
            $scope.newUser = true;

            $scope.user = {
                    transcribedName: false,
                    displayName: false,
                    email: false,
                    fullname: $scope.fullname,
                    signIn: 'Never',
                    signOut: 'Never',
                    signedIn: false,
                    song: false,
                    birthday: false,
                    imageUrl: 'https://openclipart.org/image/2400px/svg_to_png/177482/ProfilePlaceholderSuit.png'
            };
        } else {
            angular.forEach(DataFactory.users, function(user){
                console.log(user.$id, $routeParams.id);

                if (user.$id === $routeParams.id){
                    $scope.user = user;
                    $scope.user.birthday = new Date($scope.user.birthday);
                }
            });
        }



        VoiceFactory.recognition.onresult = function(result){
            $scope.user.transcribedName = result.results[0][0].transcript;
            $scope.$apply();
        };

        $scope.text = 'Record';

        $scope.startRecord = function(){
            $scope.text = 'Recording..';
            VoiceFactory.recognition.start();
        };

        $scope.endRecord = function(){
            $scope.text = 'Record';
            VoiceFactory.recognition.stop();
        };



        $scope.save = function(){

            $scope.user.birthday = new Date($scope.user.birthday).toString();

            if ($scope.user.$id){
                DataFactory.users.$save($scope.user);
            } else {
                DataFactory.users.$add($scope.user);
            }

            $location.path('/users');
        };





        $scope.search = function(){

            $http.get('https://192.168.14.53:4443/search/' + $scope.query).then(function(e){
                $scope.results = e.data;

                console.log($scope.results);
                //$scope.$apply();
            });



        };

        $scope.changeSong = function(){
            $scope.user.song = false;
        };

        $scope.saveSong = function(track){



            $scope.user.song = {
                name: track.name,
                artist: track.artists[0].name,
                date: track.date,
                uri: track.uri
            };

            if (!$scope.newUser) {
                DataFactory.users.$save($scope.user);
            }
        };



    });
