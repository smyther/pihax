'use strict';

/**
 * @ngdoc service
 * @name haxApp.DataFactory
 * @description
 * # DataFactory
 * Factory in the haxApp.
 */
angular.module('haxApp')
    .factory('DataFactory', function ($firebaseArray) {


        var users = new Firebase('https://sizzling-fire-2100.firebaseio.com/users');
        var guests = new Firebase('https://sizzling-fire-2100.firebaseio.com/guests');

        return {
            users: $firebaseArray(users),
            guests: $firebaseArray(guests)
        };

    });
