'use strict';

/**
 * @ngdoc service
 * @name haxApp.VoiceFactory
 * @description
 * # VoiceFactory
 * Factory in the haxApp.
 */
angular.module('haxApp')
    .factory('VoiceFactory', function () {

        var recognition = new webkitSpeechRecognition();
        recognition.continuous = true;
        recognition.interimResults = false;

        var speak = new SpeechSynthesisUtterance();
        var voices = window.speechSynthesis.getVoices();

        speak.voice = voices[2];
        speak.voiceURI = 'native';
        speak.volume = 1;
        speak.lang = 'en-GB';

        recognition.onresult = function(result){
          recognition.transcribedResult = result;
        };

        return {
            recognition: recognition,
            speech: speak,

            say: function (text) {
                speak.text = text;
                speechSynthesis.speak(speak);
            }

        };

    });
