'use strict';

/**
 * @ngdoc service
 * @name haxApp.DataFactory
 * @description
 * # DataFactory
 * Factory in the haxApp.
 */
angular.module('haxApp')
    .factory('SpotifyFactory', function ($timeout) {

        return {
            mopidy: null,
            present: false,
            setup: function(){

                var $this = this;

                if (!this.present){
                    // $.getScript('http://192.168.14.53:6680/mopidy/mopidy.min.js', function(){
                        $this.mopidy = new Mopidy({
                            webSocketUrl: "ws://192.168.14.53:6680/mopidy/ws/"
                        });
                        $this.present = true;
                    // });
                }

            },
            search: function(query){

                return this.mopidy.library.search({
                    "any": query
                });
            },


            getTrackFromURI: function(uri){

                console.log(uri);
                var $this = this;

                this.mopidy.library.findExact({
                    "uri": [uri]
                }).then(function(data){

                    var song;

                    angular.forEach(data, function(result){

                        console.log(result, 'eh');

                        if (result.uri.indexOf('spotify') > -1){
                            song = result.tracks[0];
                        }

                    });

                    $this.mopidy.tracklist.add([song]).then(function(list){
                       $this.mopidy.playback.play(list[0]).then(function(song){
                            $this.mopidy.playback.seek(0).then(function(song){

                                var vol = 100;
                                $this.mopidy.mixer.setVolume(vol);

                                $timeout(function(){

                                    var interval;

                                    interval = setInterval(function(){
                                        vol--;

                                        $this.mopidy.mixer.setVolume(vol);

                                        if (vol === 0){
                                            console.log('stop?');
                                            $this.mopidy.playback.stop();
                                            clearInterval(interval);
                                        }
                                    }, 20);


                                }, 8000);

                            });
                       });
                    });
                });

            }
        };



    });
