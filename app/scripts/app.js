'use strict';

/**
 * @ngdoc overview
 * @name haxApp
 * @description
 * # haxApp
 *
 * Main module of the application.
 */
angular
    .module('haxApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'firebase'
    ])
    .config(function ($routeProvider) {

        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main'
            })
            .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl',
                controllerAs: 'about'
            })

            .when('/new', {
                templateUrl: 'views/profile.html',
                controller: 'ProfileCtrl',
                newUser: true
            })

            .when('/signin', {
                templateUrl: 'views/sign-in.html',
                controller: 'SignInCtrl'
            })
            .when('/traffic', {
                templateUrl: 'views/traffic.html',
                controller: 'TrafficCtrl'
            })
            .when('/login/:id', {
                templateUrl: 'views/login.html',
                controller: 'LoginCtrl'
            })
            .when('/user/:id', {
                templateUrl: 'views/profile.html',
                controller: 'ProfileCtrl'
            })
            .when('/users', {
                templateUrl: 'views/users.html',
                controller: 'UserCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    });
